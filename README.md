# csv-to-automatic-index


## Scope
This module is designated for automatically building of philological indexes from index data in CSV format.

The selector for columns is based upon [URI Fragment Identifiers for the text/csv Media Type](https://datatracker.ietf.org/doc/html/rfc7111#section-2.2).

## Example of index data
"lemma","url","wortklasse","artikeldatum","artikeltyp"

"&","https://www.dwds.de/wb/&","Konjunktion","1976","Vollartikel"