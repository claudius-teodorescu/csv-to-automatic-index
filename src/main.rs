use csv;
use rio_api::formatter::TriplesFormatter;
use rio_api::model::{Literal, NamedNode, Triple};
use rio_turtle::TurtleFormatter;

mod config;

use clap::{Arg, Command};
use std::path::PathBuf;
use std::{env, fs};

fn main() {
    // get the CLI args
    let cli_matches = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("index_metadata_file_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(1)
                .required(true),
        )
        .arg(
            Arg::new("documents_base_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(2)
                .required(true),
        )
        .arg(Arg::new("documents_glob").index(3).required(true))
        .arg(
            Arg::new("indexes_dir_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(4)
                .required(true),
        )
        .arg(Arg::new("index_abbreviation").index(5).required(true))
        .get_matches();

    // extract the arguments for the index metadata
    let index_metadata_file_path: &PathBuf = cli_matches
        .get_one("index_metadata_file_path")
        .expect("`index_metadata_file_path`is required");
    // extract the arguments for the index documents
    let documents_base_path: &PathBuf = cli_matches
        .get_one("documents_base_path")
        .expect("`documents_base_path`is required");
    let documents_glob_arg: &String = cli_matches
        .get_one("documents_glob")
        .expect("`documents_glob`is required");
    let documents_glob: glob::Pattern =
        glob::Pattern::new(documents_glob_arg.as_str()).expect("invalid `documents_glob`");
    let indexes_dir_path: &PathBuf = cli_matches
        .get_one("indexes_dir_path")
        .expect("`indexes_dir_path`is required");
    let index_abbreviation: &String = cli_matches
        .get_one("index_abbreviation")
        .expect("`index_abbreviation`is required");

    let documents_path = documents_base_path.join(documents_glob.as_str());

    run(
        index_metadata_file_path,
        documents_path,
        indexes_dir_path,
        index_abbreviation,
    );
}

fn run(
    index_metadata_file_path: &PathBuf,
    documents_path: PathBuf,
    indexes_dir_path: &PathBuf,
    index_abbreviation: &String,
) {
    // process the index metadata
    let index_metadata = extract_index_metadata(index_metadata_file_path);

    // process the index documents
    let index_document = process_documents(documents_path, index_metadata);

    let triples_file_dir_path = indexes_dir_path.join(index_abbreviation);
    fs::create_dir_all(&triples_file_dir_path).unwrap();
    fs::write(triples_file_dir_path.join("index.ttl"), &index_document).expect("Write file.");
}

fn extract_index_metadata(index_metadata_file_path: &PathBuf) -> crate::config::Metadata {
    let index_metadata_file_contents =
        fs::read_to_string(index_metadata_file_path).expect("Failed to read config file");
    let index_metadata: crate::config::Metadata =
        crate::config::parse(&index_metadata_file_contents);

    index_metadata
}

fn process_documents(documents_path: PathBuf, index_metadata: crate::config::Metadata) -> Vec<u8> {
    let mut turtle_formatter = TurtleFormatter::new(Vec::default());

    for document_path in glob::glob(&documents_path.to_str().unwrap())
        .expect("Failed to read glob pattern")
        .filter_map(Result::ok)
    {
        process_document(
            &document_path,
            index_metadata.clone(),
            &mut turtle_formatter,
        );
    }

    let mut index_document = format!("@prefix i: <{}> .\n", crate::config::ONTOLOGY_BASE_IRI)
        .as_bytes()
        .to_vec();

    let mut triples = turtle_formatter
        .finish()
        .expect("cannot finish terms generation");
    index_document.append(&mut triples);

    index_document
}

fn process_document(
    document_path: &PathBuf,
    index_metadata: crate::config::Metadata,
    turtle_formatter: &mut TurtleFormatter<Vec<u8>>,
) {
    let mut document_reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .comment(Some(b'#'))
        .from_path(document_path)
        .expect("cannot read file");

    for record in document_reader.records() {
        let record = record.unwrap();

        // get the subject, and predicate for the current triple
        let subject = record.get(0).unwrap();
        let predicate = &index_metadata.predicate;

        // get the object(s) for the current triple(s)
        let selector = &index_metadata
            .selector
            .replace("col=", "")
            .parse::<usize>()
            .unwrap();
        let object = record.get(*selector).unwrap();

        // generate the current triple(s)
        turtle_formatter
            .format(&Triple {
                subject: NamedNode { iri: subject }.into(),
                predicate: NamedNode {
                    iri: predicate.as_str(),
                }
                .into(),
                object: Literal::Simple { value: object }.into(),
            })
            .expect("error formatting triple");
    }
}

#[test]
fn test_1() {
    let index_metadata_file_path: PathBuf = ["/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/contents/peritext/indexes/article-dates/metadata.ttl"].iter().collect();
    let documents_base_path: PathBuf = ["/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/contents/text/"].iter().collect();
    let documents_glob = glob::Pattern::new("**/*.csv").expect("wrong glob pattern");
    let documents_path = documents_base_path.join(documents_glob.as_str());
    let indexes_dir_path: PathBuf = ["/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/"].iter().collect();
    let index_abbreviation = "article-dates".to_string();

    run(
        &index_metadata_file_path,
        documents_path,
        &indexes_dir_path,
        &index_abbreviation,
    );
}

// the first column should contain the resource IRIs
// the data file should not contain header
// type of processing: all files at once, all indexes at once
