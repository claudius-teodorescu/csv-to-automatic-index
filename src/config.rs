use constcat::concat;
use sophia::api::prelude::*;
use sophia::api::term::{matcher::Any, IriRef, SimpleTerm};
use sophia::inmem::graph::LightGraph;
use sophia::turtle::parser::turtle;

pub const ONTOLOGY_BASE_IRI: &str = "https://kuberam.ro/ontologies/text-index#";
const HEADWORDS_INDEX_CLASS: &str = concat!(ONTOLOGY_BASE_IRI, "HeadwordsIndex");
const WORDS_INDEX_CLASS: &str = concat!(ONTOLOGY_BASE_IRI, "WordsIndex");
const DATES_INDEX_CLASS: &str = concat!(ONTOLOGY_BASE_IRI, "DatesIndex");
const KEYWORDS_INDEX_CLASS: &str = concat!(ONTOLOGY_BASE_IRI, "KeywordsIndex");
const SEGMENT_TYPES_INDEX_CLASS: &str = concat!(ONTOLOGY_BASE_IRI, "SegmentTypesIndex");
const PARTS_OF_SPEECH_INDEX_CLASS: &str = concat!(ONTOLOGY_BASE_IRI, "PartsOfSpeechIndex");
const TITLE_PROPERTY: &str = "title";
const SELECTOR_PROPERTY: &str = "selector";
const TYPE_PROPERTY: &str = "type";
const TEXT_BASE_IRI_PROPERTY: &str = "textBaseIRI";

#[derive(Debug, Clone)]
pub struct Metadata {
    pub title: String,
    pub selector: String,
    pub predicate: String,
    pub text_base_iri: Option<String>,
}

pub fn parse(index_metadata: &str) -> Metadata {
    // load the metadata as graph
    let mut metadata_graph: LightGraph = turtle::parse_str(index_metadata)
        .collect_triples()
        .expect("cannot extract triples");

    let title = get_object_literal_value_from_graph(&mut metadata_graph, TITLE_PROPERTY);
    let selector = get_object_literal_value_from_graph(&mut metadata_graph, SELECTOR_PROPERTY);
    let r#type = get_object_literal_value_from_graph(&mut metadata_graph, TYPE_PROPERTY);
    let text_base_url =
        get_object_literal_value_from_graph(&mut metadata_graph, TEXT_BASE_IRI_PROPERTY);

    let predicate: String = match r#type.as_str() {
        HEADWORDS_INDEX_CLASS => "i:headword".to_string(),
        WORDS_INDEX_CLASS => "i:word".to_string(),
        KEYWORDS_INDEX_CLASS => "i:keyword".to_string(),
        DATES_INDEX_CLASS => "i:date".to_string(),
        SEGMENT_TYPES_INDEX_CLASS => "i:segment_type".to_string(),
        PARTS_OF_SPEECH_INDEX_CLASS => "i:part_of_speech".to_string(),
        _ => "".to_string(),
    };

    Metadata {
        title,
        selector,
        predicate,
        text_base_iri: Some(text_base_url),
    }
}

fn get_object_literal_value_from_graph(graph: &mut LightGraph, property_name: &str) -> String {
    let mut object_value = "";
    let title_predicate = SimpleTerm::Iri(IriRef::new_unchecked(
        [ONTOLOGY_BASE_IRI, property_name].concat().into(),
    ));
    let triples = graph.triples_matching(Any, Some(title_predicate), Any);
    for triple in triples.into_iter() {
        let object_term = triple.unwrap()[2];

        match object_term {
            SimpleTerm::LiteralDatatype(value, _) => {
                object_value = value;
            }
            SimpleTerm::Iri(iri) => {
                object_value = iri.as_str();
            }
            _ => (),
        }
    }

    object_value.to_string()
}

#[test]
pub fn load_config_file() {
    let input_metadata_file_contents = r#"
    @prefix i: <https://kuberam.ro/ontologies/text-index#> .

    <https://solirom-citada.gitlab.io/data/peritext/indexes/headwords/metadata.ttl> i:metadataFor <https://solirom-citada.gitlab.io/data/peritext/indexes/headwords/index.ttl> ;
        i:type i:HeadwordsIndex ;
        i:title "Index de cuvinte-titlu" ;
        i:textBaseURL <https://solirom-citada.gitlab.io/data/text/> ;
        i:selector "/*/*[local-name() = 'orth']" .
    "#;
    let input_metadata: Metadata = parse(input_metadata_file_contents);

    println!("{:?}", input_metadata);
}
